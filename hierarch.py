#!/usr/bin/env python

import gui
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.simpledialog as simpledialog
import tkinter.filedialog as filedialog
import argparse
import proposalogger
import os

class Main(ttk.Frame):
    def __init__(self, master=None, propfn=None):
        super().__init__(master)
        self.master = master

        self.propfn = propfn
        self.proposalog = proposalogger.Proposalog(propfn) if propfn is not None else None

        self.unsaved = False
        self.status = tk.StringVar(self, 'Come, bookkeeper, friend or traitor, come')

        self.update_title()
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.create_menu()
        if self.proposalog is None: self.disable_menus()

        self.mainframe = ttk.Frame(self, borderwidth=3, relief='ridge')
        self.statusbar = ttk.Frame(self.master, borderwidth=3, relief='ridge')
        self.statuslabel = ttk.Label(self.statusbar, textvariable=self.status, anchor=tk.W)

        #self.mainframe.grid(row=0, column=0, sticky=tk.N)
        self.mainframe.pack(side='top', fill=tk.BOTH, expand=True)

        #self.statusbar.grid(row=1, column=0, sticky=tk.S+tk.W)
        self.statusbar.pack(side='bottom', fill=tk.X)
        self.statuslabel.pack(expand=True, fill=tk.X, anchor=tk.W)

    def clear(self):
        self.mainframe.destroy()
        self.mainframe = ttk.Frame(self)
        self.mainframe.pack(side='top', fill=tk.BOTH, expand=True)
        #self.mainframe = ttk.Frame(self, borderwidth=3, relief='ridge')
        #self.mainframe.grid(row=0, sticky=tk.N)

    def create_menu(self):
        self.menu = tk.Menu(self.master)
        self.master.config(menu=self.menu)
        filemenu = tk.Menu(self.menu, tearoff=0)
        #self.master.bind('<Alt_L><f>', lambda x: filemenu.post(0,0))
        #self.master.bind('<Escape>', lambda x: filemenu.destroy())
        filemenu.add_command(label='New', command=self.newdb)
        filemenu.add_command(label='Open', command=self.opendb)
        filemenu.add_command(label='Save', command=self.savedb)
        #filemenu.add_command(label='Save as', command=self.saveasdb)
        #filemenu.add_command(label='Exit', command=self.exit, accelerator='q')
        filemenu.add_command(label='Exit', command=self.exit)

        #council menu:
        #edit types
        #edit voters
        #edit propositions
        self.councilmenu = tk.Menu(self.menu, tearoff=0)
        self.councilmenu.add_command(label='Seats', command=self.counciltypes)
        self.councilmenu.add_command(label='Members', command=self.councilvoters)
        self.councilmenu.add_command(label='Proposals', command=self.councilprops)
        self.councilmenu.add_command(label='Voting', command=self.councilvoting)

        #ministry menu:
        #edit gods
        #edit ministries
        #edit loyalty
        #edit budgets
        self.ministrymenu = tk.Menu(self.menu, tearoff=0)
        self.ministrymenu.add_command(label='Heads', command=self.ministryheads)
        self.ministrymenu.add_command(label='Ministries', command=self.ministryministries)
        self.ministrymenu.add_command(label='Loyalty', command=self.ministryloyalty)
        self.ministrymenu.add_command(label='Budgets', command=self.ministrybudgets)

        #reports menu:
        #turn update editor
        #ministry update editor
        #finance report editor
        #proposal registry
        #compile update
        self.reportmenu = tk.Menu(self.menu, tearoff=0)
        self.reportmenu.add_command(label='Turn update', command=self.reportupdate)
        self.reportmenu.add_command(label='Ministry events', command=self.reportevents)
        self.reportmenu.add_command(label='Finance report', command=self.reportfinance)
        self.reportmenu.add_command(label='Proposal summary', command=self.reportproposals)
        self.reportmenu.add_command(label='Compile', command=self.reportcompile)

        #halp menu
        halpmenu = tk.Menu(self.menu, tearoff=0)
        halpmenu.add_command(label='About', command=self.about)

        self.menu.add_cascade(label='File', menu=filemenu)
        self.menu.add_cascade(label='Council', menu=self.councilmenu)
        self.menu.add_cascade(label='Ministry', menu=self.ministrymenu)
        self.menu.add_cascade(label='Reports', menu=self.reportmenu)
        self.menu.add_cascade(label='Halp!', menu=halpmenu)

    def newdb(self):
        #prompt if unsaved
        propfn = filedialog.asksaveasfilename(title='New proposalog')
        if not propfn: return
        self.propfn = propfn
        self.proposalog = proposalogger.Proposalog(self.propfn)
        self.update_title()
        self.enable_menus()
    def opendb(self):
        #prompt if unsaved
        propfn = filedialog.askopenfilename(title='New proposalog')
        if not propfn: return
        self.propfn = propfn
        self.proposalog = proposalogger.Proposalog(self.propfn)
        self.update_title()
        self.enable_menus()
    def savedb(self):
        self.proposalog.save()
    def exit(self):
        #prompt if unsaved
        self.quit()

    def disable_menus(self):
        self.menu.entryconfig('Council', state='disabled')
        self.menu.entryconfig('Ministry', state='disabled')
        self.menu.entryconfig('Reports', state='disabled')
    def enable_menus(self):
        self.menu.entryconfig('Council', state='normal')
        self.menu.entryconfig('Ministry', state='normal')
        self.menu.entryconfig('Reports', state='normal')

    def update_title(self):
        if self.propfn:
            self.master.title('{} - Clockwork Angel'.format(os.path.basename(self.propfn)))
        else: self.master.title('Clockwork Angel')

    def counciltypes(self): 
        #prompt if unsaved
        self.clear()
        gui.council.SeatManager(self.mainframe, self.proposalog).pack()
    def councilvoters(self): 
        #prompt if unsaved
        self.clear()
        gui.council.MemberManager(self.mainframe, self.proposalog).pack()
    def councilprops(self): 
        #prompt if unsaved
        self.clear()
        gui.council.ProposalManager(self.mainframe, self.proposalog).pack()
    def councilvoting(self):
        #prompt if unsaved
        self.clear()
        gui.voting.VoteManager(self.mainframe, self.proposalog).pack()

    def ministryheads(self): pass
    def ministryministries(self): pass
    def ministryloyalty(self): pass
    def ministrybudgets(self): pass

    def reportupdate(self): pass
    def reportevents(self): pass
    def reportfinance(self): pass
    def reportproposals(self): pass
    def reportcompile(self): pass

    def about(self): pass
def main(dbfile=None):
    root = tk.Tk()
    app = Main(root, dbfile)
    root.geometry('680x720')
    app.mainloop()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('dbfile', nargs='?')

    args = parser.parse_args()

    main(args.dbfile)
