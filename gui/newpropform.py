import tkinter as tk
import tkinter.ttk as ttk
#import proposalogger

class NewProposalForm(ttk.Frame):
    def __init__(self, master=None, proposalog=None):
        super().__init__(master)
        self.master = master

        self.propcode = tk.StringVar(self)
        self.propname = tk.StringVar(self)
        self.propdescription = tk.StringVar(self)
        self.proposalog = proposalog

        self.propmover = tk.StringVar(self)
        self.propseconder = tk.StringVar(self)
        self.proptime = tk.StringVar(self)

        self.grid(row=0, column=0)
        self.create_widgets()


    def create_widgets(self):
        #self.label_title = ttk.Label(self, text='Create a new proposal')
        #self.frame = ttk.Frame(self, borderwidth=5, relief='ridge')
        self.nameframe = ttk.Frame(self)
        self.label_code = ttk.Label(self.nameframe, text='Code:')

        self.input_code = ttk.Entry(self.nameframe, width=8, textvariable=self.propcode)

        self.label_name = ttk.Label(self.nameframe, text='Name:')
        self.input_name = ttk.Entry(self.nameframe, width=60, textvariable=self.propname)

        self.metadataframe = ttk.Frame(self)
        self.label_time = ttk.Label(self.metadataframe, text='Proposed on:')
        self.input_time = ttk.Entry(self.metadataframe, textvariable=self.proptime)
        self.label_mover = ttk.Label(self.metadataframe, text='Proposed by:')
        voternames = [voter.name for voter in self.proposalog.get_voters()]
        self.input_mover = ttk.OptionMenu(self.metadataframe, self.propmover, 'Choose a proposer', *voternames)
        self.label_seconder = ttk.Label(self.metadataframe, text='Seconded by:')
        self.input_seconder = ttk.OptionMenu(self.metadataframe, self.propseconder, 'Choose a seconder', *voternames)

        self.descframe = ttk.Frame(self)
        self.label_description = ttk.Label(self.descframe, text='Description:')
        self.input_description = tk.Text(self.descframe, width=80, height=15)

        self.buttonframe = ttk.Frame(self)
        self.button_create = ttk.Button(self.buttonframe, text='Save', command=self.save)
        self.button_cancel = ttk.Button(self.buttonframe, text='Cancel', command=self.cancel)

        #self.label_title.grid(row=0, column=0, columnspan=4)
        #self.frame.grid(row=0, column=0, rowspan=2, columnspan=4)
        self.nameframe.grid(row=0, sticky=tk.W)
        self.label_code.grid(row=0, column=0)
        self.input_code.grid(row=0, column=1)
        self.label_name.grid(row=0, column=2)
        self.input_name.grid(row=0, column=3)

        #metadata: time, mover, seconder
        self.metadataframe.grid(row=1, sticky=tk.W)
        self.label_time.grid(row=0, column=0)
        self.input_time.grid(row=0, column=1)
        self.label_mover.grid(row=1, column=0)
        self.input_mover.grid(row=1, column=1)
        self.label_seconder.grid(row=2, column=0)
        self.input_seconder.grid(row=2, column=1)

        #desc: description
        self.descframe.grid(row=2, sticky=tk.W)
        self.label_description.grid(row=0, column=0, sticky=tk.N)
        self.input_description.grid(row=0, column=1)
        #buttons
        self.buttonframe.grid(row=3)
        self.button_create.grid(row=0, column=0)
        self.button_cancel.grid(row=0, column=1)
    def save(self):
        print('code = {}'.format(self.propcode))
        print('name = {}'.format(self.propname))
        print('desc = {}'.format(self.propdescription))

    def cancel(self):
        dialogwindow = tk.Toplevel(self)
        confirmation = Prompt(dialogwindow, message='Confirm exiting?')
        confirmation.mainloop()
        dialogwindow.destroy()
        if confirmation.promptresult: 
            self.destroy()
            self.quit()

class Prompt(ttk.Frame):
    def __init__(self, master=None, message=''):
        super().__init__(master)
        self.master = master

        self.message = message

        self.promptresult = tk.BooleanVar(value=False)

        self.pack()
        self.create_widgets()

    def create_widgets(self):
        label_message = ttk.Label(self, text=self.message)
        button_ok = ttk.Button(self, text='OK', command=self.ok)
        button_cancel = ttk.Button(self, text='Cancel', command=self.cancel)

        label_message.pack(side='top')
        button_ok.pack(side='left')
        button_cancel.pack(side='right')

    def ok(self): 
        self.promptresult = True
        self.destroy()
        self.quit()
    def cancel(self): 
        self.promptresult = False 
        self.destroy()
        self.quit()
