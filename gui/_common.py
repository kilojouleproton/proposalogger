import tkinter as tk
import tkinter.ttk as ttk

class MovableList(ttk.Frame):
    def __init__(self, master=None, itemtype=None):
        super().__init__(master)
        self.master = master

        self.items = []
        self.itemtype = itemtype
        self.callbacks = {
                'add': self.add_item,
                'remove': self.remove_item,
                'move': self.move_item
        }

        self.pack()
        self.listframe = ttk.Frame(self)

    def create_widgets(self):
        #make sure to pack listframe if overwriting create_widgets
        self.listframe.pack()
        #calls to load methods go here (if applicable)

    def add_item(self, index=None, **kwargs):
        if index is None: row = len(self.items)
        else:
            row = index + 1
            for item in self.items[index+1:]:
                item.row += 1
                item.regrid()
        newitem = self.itemtype(self.listframe, row, self.callbacks, **kwargs)
        self.items.insert(row, newitem)
        newitem.grid(row=row)

    def move_item(self, index, offset):
        if offset == 0: return
        elif len(self.items) < 2: return

        endpoint = min(max(0, index + offset), len(self.items))
        self.items[index].row = endpoint
        self.items[endpoint].row = index
        self.items[index].regrid()
        self.items[endpoint].regrid()
        self.items[index], self.items[endpoint] = self.items[endpoint], self.items[index]

    def remove_item(self, index=None):
        if len(self.items) == 0: return
        if index is None: index = len(self.items) - 1
        for item in self.items[index+1:]:
            item.row -= 1
            item.regrid()

        self.items[index].destroy()
        self.items.pop(index)

class MovableListItem(ttk.Frame):
    def __init__(self, master, row, callbacks):
        super().__init__(master)
        self.master = master
        self.row = row
        self.fn_add = callbacks['add']
        self.fn_remove = callbacks['remove']
        self.fn_move = callbacks['move']
        self.hidden = False

    def hide(self):
        if self.hidden: return 0
        #for widget in self.winfo_children(): widget.grid_remove()
        self.grid_remove()
        self.hidden = True
        return 1

    def show(self):
        if not self.hidden: return 0

        #for widget in self.winfo_children():
            #self.grid_configure(in_=self)
        self.grid_configure(in_=self.master)
        self.hidden = False
        return 1

    def create_widgets(self):
        self.grid()

    def regrid(self): self.grid_configure(row=self.row)
