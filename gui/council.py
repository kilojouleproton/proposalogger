import tkinter as tk
import tkinter.ttk as ttk
from . import _common

class SeatType(_common.MovableListItem):
    def __init__(self, master, row, callbacks, prefix='', name='', suffix=''):
        super().__init__(master, row, callbacks)

        self.prefix = tk.StringVar(value=prefix)
        self.name = tk.StringVar(value=name)
        self.suffix = tk.StringVar(value=suffix)

        self.grid()
        self.create_widgets()
    def create_widgets(self):
        ttk.Entry(self, textvariable=self.prefix).grid(row=0, column=0)
        ttk.Entry(self, textvariable=self.name).grid(row=0, column=1)
        ttk.Entry(self, textvariable=self.suffix).grid(row=0, column=2)
        ttk.Button(self, text='+', width=3, command=lambda: self.fn_add(self.row)).grid(row=0, column=3)
        ttk.Button(self, text='-', width=3, command=lambda: self.fn_remove(self.row)).grid(row=0, column=4)
        ttk.Button(self, text='^', width=3, command=lambda: self.fn_move(self.row, -1)).grid(row=0, column=5)
        ttk.Button(self, text='v', width=3, command=lambda: self.fn_move(self.row, 1)).grid(row=0, column=6)

class SeatManager(_common.MovableList):
    def __init__(self, master=None, proposalog=None, itemtype=SeatType):
        super().__init__(master, itemtype)
        self.master = master

        self.proposalog = proposalog
        self.types = []

        #necessary? unnecessary?
        #self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.typeframe = ttk.Frame(self)

        buttonframe = ttk.Frame(self)
        ttk.Button(buttonframe, text='Add type', command=lambda: self.add_item()).grid(row=0, column=0)
        ttk.Button(buttonframe, text='Remove type', command=lambda: self.remove_item()).grid(row=0, column=1)
        ttk.Separator(buttonframe, orient=tk.VERTICAL).grid(
                row=0, column=2)
        ttk.Button(buttonframe, text='Save', command=lambda: self.save()).grid(
                row=0, column=3)
        ttk.Button(buttonframe, text='Revert', command=lambda: self.revert()).grid(
                row=0, column=4)

        buttonframe.pack()
        ttk.Separator(self, orient=tk.HORIZONTAL).pack()
        self.listframe.pack()
        self.load()


    def load(self):
        for rawtype in self.proposalog.get_types():
            self.add_item(prefix=rawtype.prefix, name=rawtype.name, suffix=rawtype.suffix)
    def revert(self):
        #prompt if unsaved
        for item in self.items: item.destroy()
        for i in range(len(self.types)-1, -1): self.items[i].pop()
        self.load()

    def save(self):
        self.proposalog.truncate_table('types')
        for item in self.items:
            self.proposalog.add_type(name=item.name.get(),
                    prefix=item.prefix.get(),
                    suffix=item.suffix.get()
            )
        self.proposalog.save()
        self.master.master.status.set('Saved seat types successfully')
        self.master.unsaved = False

class Member(_common.MovableListItem):
    def __init__(self, master, row, callbacks, type='', name='', types=None, active=True, default_type=''):
        super().__init__(master, row, callbacks)

        self.types = types
        self.default_type = default_type
        if self.types and not self.default_type: self.default_type = self.types[0]
        self.type = tk.StringVar(self, value=type)
        self.name = tk.StringVar(self, value=name)
        self.active = tk.BooleanVar(self, value=active)

        self.grid()
        self.create_widgets()
    def create_widgets(self):
        #ttk.Entry(self, textvariable=self.type).grid(row=0, column=0)
        if not self.type.get(): self.type.set(self.default_type)
        ttk.OptionMenu(self, self.type, self.type.get(), *self.types).grid(row=0, column=0)
        ttk.Entry(self, textvariable=self.name).grid(row=0, column=1)
        ttk.Checkbutton(self, variable=self.active, text='Active?').grid(row=0, column=2)
        ttk.Button(self, text='+', width=3, command=lambda: self.fn_add(self.row, type='', name='', active=True, types=self.types)).grid(row=0, column=3)
        ttk.Button(self, text='-', width=3, command=lambda: self.fn_remove(self.row)).grid(row=0, column=4)
        ttk.Button(self, text='^', width=3, command=lambda: self.fn_move(self.row, -1)).grid(row=0, column=5)
        ttk.Button(self, text='v', width=3, command=lambda: self.fn_move(self.row, 1)).grid(row=0, column=6)

class MemberManager(_common.MovableList):
    def __init__(self, master=None, proposalog=None, itemtype=Member):
        super().__init__(master, itemtype)

        self.proposalog = proposalog
        self.types = [t.name for t in proposalog.get_types()]
        self.default_type = self.types[0] if self.types else ''

        #necessary? unnecessary?
        #self.pack()
        self.create_widgets()

    def create_widgets(self):
        buttonframe = ttk.Frame(self)
        ttk.Button(buttonframe, text='Add member', command=lambda: self.add_item(type='', name='', active=True, types=self.types)).grid(
                row=0, column=0)
        ttk.Button(buttonframe, text='Remove member', command=lambda: self.remove_item()).grid(
                row=0, column=1)
        #separator
        ttk.Button(buttonframe, text='Save', command=lambda: self.save()).grid(
                row=0, column=2)
        ttk.Button(buttonframe, text='Revert', command=lambda: self.revert()).grid(
                row=0, column=3)

        buttonframe.pack()

        self.listframe.pack()

        self.load()

    def load(self):
        for member in self.proposalog.get_voters():
            self.add_item(type=member.type, name=member.name, active=member.active, types=self.types)

    def revert(self):
        #prompt if unsaved
        for t in self.items: t.destroy()
        for i in range(len(self.items)-1, -1): self.items[i].pop()
        self.load()
    def save(self):
        self.proposalog.truncate_table('voters')
        for item in self.items:
            self.proposalog.add_voter(item.name.get(),
                    type=item.type.get(),
                    active=item.active.get(),
            )
        self.proposalog.save()
        self.master.master.status.set('Saved Council members successfully')
        self.master.unsaved = False

class Proposal(_common.MovableListItem):
    def __init__(self, master, row, callbacks, proposalog=None, code='', name='', description='', mover='', seconder='', time='', voters=None):
        super().__init__(master, row, callbacks)

        self.proposalog = proposalog
        self.code = tk.StringVar(self, value=code)
        self.name = tk.StringVar(self, value=name)
        self.mover= tk.StringVar(self, value=mover)
        self.seconder = tk.StringVar(self, value=seconder)
        self.description = tk.StringVar(self, value=description)
        self.time = tk.StringVar(self, value=str(time))

        self.voters = [] if voters is None else voters

        self.grid()
        self.create_widgets()

    def get_description(self): 
        return self.input_description.get('1.0', tk.END).rstrip()

    def create_widgets(self):
        ttk.Label(self, text='Code:').grid(
                row=0, column=0)
        ttk.Entry(self, textvariable=self.code, width=10).grid(
                row=0, column=1)
        ttk.Label(self, text='Name:').grid(
                row=0, column=2)
        ttk.Entry(self, textvariable=self.name, width=35).grid(
                row=0, column=3)

        ttk.Label(self, text='Proposed by:').grid(
                row=1, column=0)
        #ttk.Entry(self, textvariable=self.mover, width=10).grid(
        mover = ttk.OptionMenu(self, self.mover, self.mover.get(), '', *self.voters)
        mover.grid(
                row=1, column=1, sticky=tk.W+tk.E, columnspan=2)
        mover.configure(width=15)
        squeezetime = ttk.Frame(self)
        squeezetime.grid(row=1, column=2, columnspan=2)

        ttk.Label(squeezetime, text='on turn').pack(
                side='left', anchor=tk.W)
        ttk.Entry(squeezetime, textvariable=self.time, width=4).pack(
                side='left', anchor=tk.W)

        ttk.Label(self, text='Seconded by:').grid(
                row=2, column=0)
        #ttk.Entry(self, textvariable=self.seconder, width=10).grid(
        seconder = ttk.OptionMenu(self, self.seconder, self.seconder.get(), '', *self.voters)
        seconder.grid(
                row=2, column=1, sticky=tk.W+tk.E, columnspan=2)
        seconder.configure(width=15)

        ttk.Label(self, text='Description:').grid(
                row=3, column=0)

        descbox = ttk.Frame(self)
        descbox.grid(row=3, column=1, columnspan=7)
        desc_scrollbar = ttk.Scrollbar(descbox)
        desc_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.input_description = tk.Text(descbox, undo=True, wrap=tk.WORD, height=3, yscrollcommand=desc_scrollbar.set)
        desc_scrollbar.config(command=self.input_description.yview)
        self.input_description.insert(tk.INSERT, self.description.get())
        self.input_description.pack()

        ttk.Button(self, text='+', width=3, command=lambda: self.fn_add(self.row, time=self.time.get(), voters=self.voters)).grid(
                row=0, column=4)
        ttk.Button(self, text='-', width=3, command=lambda: self.fn_remove(self.row)).grid(
                row=0, column=5)
        ttk.Button(self, text='^', width=3, command=lambda: self.fn_move(self.row, -1)).grid(
                row=0, column=6)
        ttk.Button(self, text='v', width=3, command=lambda: self.fn_move(self.row, 1)).grid(
                row=0, column=7)

class ProposalManager(_common.MovableList):
    def __init__(self, master=None, proposalog=None, itemtype=Proposal, time=0.0):
        super().__init__(master, itemtype)

        self.proposalog = proposalog
        self.time = time
        self.voters = [voter.name for voter in self.proposalog.get_voters()]

        self.create_widgets()

    def create_widgets(self):
        buttonframe = ttk.Frame(self)
        ttk.Button(buttonframe, text='Add proposal', command=lambda: self.add_item(time=self.time, voters=self.voters)).grid(
                row=0, column=0)
        ttk.Button(buttonframe, text='Remove proposal', command=lambda: self.remove_item()).grid(
                row=0, column=1)
        #separator
        ttk.Button(buttonframe, text='Save', command=lambda: self.save()).grid(
                row=0, column=2)
        ttk.Button(buttonframe, text='Revert', command=lambda: self.revert()).grid(
                row=0, column=3)

        buttonframe.pack(side='top')
        self.filterframe = ttk.Frame(self)
        self.create_filter_controls()
        ttk.Separator(self, orient=tk.HORIZONTAL).pack(expand=True, fill=tk.X, pady=2)
        self.filterframe.pack()
        ttk.Separator(self, orient=tk.HORIZONTAL).pack(expand=True, fill=tk.X, pady=2)

        self.listframe.pack()

        self.load()

    def create_filter_controls(self):
        self.filterstart = tk.StringVar(self.filterframe)
        self.filterstop = tk.StringVar(self.filterframe)
        self.filtercode = tk.StringVar(self.filterframe)
        self.filtername = tk.StringVar(self.filterframe)
        ttk.Label(self.filterframe, text='Time:').pack(side='left')
        self.input_filterstart = ttk.Entry(self.filterframe, textvariable=self.filterstart, width=4)
        self.input_filterstart.pack(side='left')

        ttk.Label(self.filterframe, text='-').pack(side='left')
        self.input_filterstop = ttk.Entry(self.filterframe, textvariable=self.filterstop, width=4)
        self.input_filterstop.pack(side='left')

        ttk.Separator(self.filterframe, orient=tk.VERTICAL).pack(side='left')

        ttk.Label(self.filterframe, text='Code:').pack(side='left')
        self.input_filtercode = ttk.Entry(self.filterframe, textvariable=self.filtercode, width=10)
        self.input_filtercode.pack(side='left')

        ttk.Label(self.filterframe, text='Name:').pack(side='left')
        self.input_filtername = ttk.Entry(self.filterframe, textvariable=self.filtername, width=20)
        self.input_filtername.pack(side='left')

        ttk.Separator(self.filterframe, orient=tk.VERTICAL).pack(side='left')

        ttk.Button(self.filterframe, text='Filter', command=self.filter).pack(side='left')

    def filter(self):
        start = self.filterstart.get()
        start = float(start) if start else None
        stop = self.filterstop.get()
        stop = float(stop) if stop else None
        code = self.filtercode.get()
        name = self.filtername.get()

        for item in self.items:
            if start is not None or stop is not None:
                time = item.time.get()
                if not time: 
                    nhidden += item.hide()
                    continue
                time = float(time)

                if start is not None and time < start: 
                    item.hide()
                    continue
                elif stop is not None and time > stop: 
                    item.hide()
                    continue
            if code:
                itemcode = item.code.get()
                if code not in itemcode:
                    item.hide()
                    continue
            if name:
                itemname = item.name.get()
                if name not in itemname:
                    item.hide()
                    continue
            item.show()
        nhidden = 0
        nshown = 0
        for item in self.items:
            if item.hidden: nhidden += 1
            else: nshown += 1

        self.master.master.status.set('{}/{} proposals visible'.format(nshown, len(self.items)))



    def load(self):
        for row in self.proposalog.get_propids(decay=0, include_dead=True):
            proposal = self.proposalog.get_proposal(row[0])
            try: mover = self.proposalog.get_voter(proposal.mover).name
            except KeyError: mover = ''
            try: seconder = self.proposalog.get_voter(proposal.seconder).name
            except KeyError: seconder = ''
            self.add_item(code=proposal.code, name=proposal.name, 
                    mover=mover, seconder=seconder,
                    description=proposal.description, time=proposal.time,
                    voters=self.voters,
                )

    def revert(self):
        #prompt if unsaved
        for t in self.items: t.destroy()
        for i in range(len(self.items)-1, -1): self.items[i].pop()
        self.load()

    def save(self):
        self.proposalog.truncate_table('proposals')
        for item in self.items:
            self.proposalog.add_proposal(
                    code=item.code.get(), name=item.name.get(),
                    description=item.get_description(),
                    time=float(item.time.get()),
                    mover=item.mover.get(),
                    seconder=item.seconder.get(),
                    status=1,
            )
        self.proposalog.save()
        self.master.master.status.set('Saved proposals successfully')
        self.master.unsaved = False

    #def add_item(self, *args, **kwargs):
    #    super().add_item(*args, **kwargs)
    #    print(args, kwargs)
    #    print(self.items)

