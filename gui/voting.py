import tkinter as tk
import tkinter.ttk as ttk
import tkinter.font as tkfont

class VoteManager(ttk.Frame):
    def __init__(self, master, proposalog):
        super().__init__(master)
        self.master = master

        self.proposalog = proposalog
        self.proposals = self.proposalog.get_proposals()
        _propnames = ['{}: {} {}'.format(proposal.time, proposal.code, proposal.name) for proposal in self.proposals]
        self.propnames = tk.StringVar(self)
        self.propnames.set(_propnames)

        self.voters = self.proposalog.get_voters()
        self.voternames = [voter.name for voter in self.voters]

        self.create_widgets()

    def create_widgets(self):
        self.pack(side=tk.LEFT, fill=tk.BOTH, anchor=tk.W)
        self.listframe = ttk.Frame(self)
        self.listframe.pack(side=tk.LEFT, anchor=tk.W, fill=tk.BOTH)

        self.propframe = ttk.Frame(self)
        self.propframe.pack(side=tk.LEFT, fill=tk.BOTH)

        self.var_curpropname = tk.StringVar(self, value='')
        self.var_curpropdesc = tk.StringVar(self, value='')
        self.var_curpropmover = tk.StringVar(self, value='')
        self.var_curpropseconder = tk.StringVar(self, value='')

        self.curpropname = ttk.Label(self.propframe, textvariable=self.var_curpropname, font=tkfont.Font(size=14), wraplength=450)
        self.curpropname.grid(row=0, sticky=tk.N)

        self.curpropdesc = ttk.Label(self.propframe, textvariable=self.var_curpropdesc, wraplength=450, font=tkfont.Font(size=7))
        self.curpropdesc.grid(row=1, sticky=tk.W)

        moverbox = ttk.Frame(self.propframe)
        moverbox.grid(row=2, sticky=tk.W)
        ttk.Label(moverbox, text='Proposed by:', font=tkfont.Font(size=7)).grid(row=0, column=0, sticky=tk.W)
        self.curpropmover = ttk.OptionMenu(moverbox, self.var_curpropmover, self.var_curpropmover.get(), '', *self.voternames)
        self.curpropmover.config(width=20)
        self.curpropmover.grid(row=0, column=1, sticky=tk.W)

        ttk.Label(moverbox, text='Seconded by:', font=tkfont.Font(size=7)).grid(row=1, column=0, sticky=tk.W)
        self.curpropseconder = ttk.OptionMenu(moverbox, self.var_curpropseconder, self.var_curpropseconder.get(), '', *self.voternames)
        self.curpropseconder.config(width=20)
        self.curpropseconder.grid(row=1, column=1, sticky=tk.W)

        pl_scrollbar = ttk.Scrollbar(self.listframe)
        pl_scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.proplist = tk.Listbox(self.listframe, listvariable=self.propnames,
                font=tkfont.Font(size=7),
                height=400,
                yscrollcommand=pl_scrollbar.set,
                width=30
        )
        self.proplist.bind('<<ListboxSelect>>', self.select_proposal, True)
        pl_scrollbar.config(command=self.proplist.yview)
        self.proplist.pack(fill=tk.Y, expand=True)

        self.yeastr = tk.StringVar()
        self.naystr = tk.StringVar()
        self.mehstr = tk.StringVar()

        self.voteframe = ttk.Frame(self.propframe)
        self.voteframe.grid(row=3)
        self.create_votes()

        self.update_summary()
        buttonframe = ttk.Frame(self.propframe)
        buttonframe.grid(row=4)

        ttk.Button(buttonframe, text='Save', command=self.save).pack(side=tk.LEFT)
        ttk.Button(buttonframe, text='Revert', command=self.revert).pack(side=tk.LEFT)

        
    def select_proposal(self, event):
        if not self.proplist.curselection(): return
        index = self.proplist.curselection()[0]
        self.proposal = self.proposals[index]

        self.var_curpropname.set('{} {}'.format(self.proposal.code, self.proposal.name))
        self.var_curpropdesc.set(self.proposal.description)
        self.var_curpropmover.set(self.proposalog.get_voter(self.proposal.mover).name)
        try: self.var_curpropseconder.set(self.proposalog.get_voter(self.proposal.seconder).name)
        except KeyError: self.var_curpropseconder.set('')
        for i, voter in enumerate(self.voters):
            vote = self.proposalog.get_votes(self.proposal, voter)
            if not vote: self.var_votes[i].set(-999)
            elif vote is None: self.var_votes[i].set(-999)
            else:
                vote = vote[0]
                if vote is None: self.var_votes[i].set(-999)
                elif vote > 0: self.var_votes[i].set(1)
                elif vote < 0: self.var_votes[i].set(-1)
                else: self.var_votes[i].set(0)
        self.update_summary()

    def create_votes(self):
        self.var_votes = []
        offset = 1
        ttk.Label(self.voteframe, text='?').grid(row=0, column=1)
        ttk.Label(self.voteframe, text='Y').grid(row=0, column=2)
        ttk.Label(self.voteframe, text='N').grid(row=0, column=3)
        ttk.Label(self.voteframe, text='A').grid(row=0, column=4)
        for i, voter in enumerate(self.voters):
            #vote = self.proposalog.get_votes(self.proposal, voter)
            #print(vote)

            var_vote = tk.IntVar()
            var_vote.set(-999)
            name = ttk.Label(self.voteframe, text=voter.name, width=40, font=tkfont.Font(size=8))
            name.grid(row=i+offset, column=0, sticky=tk.W)
            unvote = ttk.Radiobutton(self.voteframe, variable=var_vote, value=-999, command=self.update_summary)
            unvote.grid(row=i+offset, column=1)
            yea = ttk.Radiobutton(self.voteframe, variable=var_vote, value=1, command=self.update_summary)
            yea.grid(row=i+offset, column=2)
            nay = ttk.Radiobutton(self.voteframe, variable=var_vote, value=-1, command=self.update_summary)
            nay.grid(row=i+offset, column=3)
            abstain = ttk.Radiobutton(self.voteframe, variable=var_vote, value=0, command=self.update_summary)
            abstain.grid(row=i+offset, column=4)
            self.var_votes.append(var_vote)
        ttk.Label(self.voteframe, textvariable=self.yeastr).grid(row=i+1+offset, column=2)
        ttk.Label(self.voteframe, textvariable=self.naystr).grid(row=i+1+offset, column=3)
        ttk.Label(self.voteframe, textvariable=self.mehstr).grid(row=i+1+offset, column=4)

    def update_summary(self):
        yea = 0
        nay = 0
        meh = 0
        for votevar in self.var_votes:
            vote = votevar.get()
            if vote == -999: meh += 1
            elif vote > 0: yea += 1
            elif vote < 0: nay += 1
            else: meh += 1
        yeastr = str(yea)
        naystr = str(nay)
        mehstr = str(meh)
        if yea > 0: yeastr = '+' + yeastr
        if nay > 0: naystr = '-' + naystr

        self.yeastr.set(yeastr)
        self.naystr.set(naystr)
        self.mehstr.set(mehstr)

    def save(self): 
        propid = self.proposal.id
        for i, rawvote in enumerate(self.var_votes):
            vote = rawvote.get()
            vote = None if vote == -999 else vote
            #if vote is None: continue
            self.proposalog.amend_vote(self.proposal, self.voters[i], vote)
        self.proposalog.save()
    def revert(self): self.select_proposal(1)
