import tkinter as tk
import tkinter.ttk as ttk

class ProposalVotingForm(ttk.Frame):
	def __init__(self, master=None, proposal=None, proposalog=None):
		if proposal is None: raise TypeError('No proposal given')
		if proposalog is None: raise TypeError('No proposalog given')
		super().__init__(master)
		self.master = master

		self.proposal = proposal
		self.proposalog = proposalog
		self.proptime = tk.DoubleVar(self, value=proposal.time)
		self.propmover = tk.StringVar(self, value=self.proposalog.get_voter(proposal.mover).name)
		self.propseconder = tk.StringVar(value=self.proposalog.get_voter(proposal.seconder).name)

		self.var_yea = tk.StringVar(self)
		self.var_nay = tk.StringVar(self, value='1')
		self.var_abstain = tk.StringVar(self)

		self.label_voters = []
		self.input_labels = []
		self.input_votes = []
		self.var_votes = []

		self.grid(row=0, column=0)
		self.create_widgets()

	def create_widgets(self):
		self.label_name = ttk.Label(self, text='Proposal {}: {}'.format(self.proposal.code, self.proposal.name))

		self.label_desc = ttk.Label(self, text=self.proposal.description, wraplength=480)

		propdetails = tk.Frame(self)
		self.label_time = ttk.Label(propdetails, text='Proposed on:')
		self.input_time = ttk.Entry(propdetails, textvariable=self.proptime)

		self.label_mover = ttk.Label(propdetails, text='Proposed by:')
		self.input_mover = ttk.Entry(propdetails, textvariable=self.propmover)
		self.label_seconder = ttk.Label(propdetails, text='Seconded by:')
		self.input_seconder = ttk.Entry(propdetails, textvariable=self.propseconder)


		self.label_name.grid(row=0, column=0, columnspan=5, pady=8)
		self.label_desc.grid(row=1, column=0, columnspan=5)

		propdetails.grid(row=2, column=0)
		self.label_time.grid(row=0, column=0)
		self.input_time.grid(row=0, column=1)

		self.label_mover.grid(row=1, column=0)
		self.input_mover.grid(row=1, column=1)
		self.label_seconder.grid(row=2, column=0)
		self.input_seconder.grid(row=2, column=1)

		firstrow = 3


		by_type = {}
		for voter in self.proposalog.get_voters():
			if voter.type not in by_type: by_type[voter.type] = []
			by_type[voter.type].append(voter)

		offset = 0
		for typeid in by_type:

			t = self.proposalog.get_type(typeid)

			label = ttk.Label(self, text=t.name)
			label.grid(row=firstrow+offset, column=0, pady=8)
			#label_null = ttk.Label(self, text=' ')
			label_yea = ttk.Label(self, text='Y')
			label_nay = ttk.Label(self, text='N')
			label_abstain = ttk.Label(self, text='A')
			#self.label_null.grid(row=5, column=1)
			label_yea.grid(row=firstrow+offset, column=3)
			label_nay.grid(row=firstrow+offset, column=4)
			label_abstain.grid(row=firstrow+offset, column=5)

			offset += 1

			sep = ttk.Separator(self, orient=tk.HORIZONTAL)
			sep.grid(row=firstrow+offset, column=0, sticky=tk.E+tk.W, columnspan=6)
			offset += 1

			for voter in by_type[typeid]:
				label = ttk.Label(self, text='{}{}{}'.format(t.prefix.title(), voter.name, t.suffix))
				label.grid(row=firstrow+offset, column=0)

				self.label_voters.append(label)

				vote = self.proposalog.get_votes(self.proposal, voter)
				if vote is None: default = -999
				else: default = vote[0]
				var = tk.IntVar(self, value=default)
				unvote = ttk.Radiobutton(self, variable=var, value=-999, command=self.update_totals)
				yeavote = ttk.Radiobutton(self, variable=var, value=1, command=self.update_totals)
				nayvote = ttk.Radiobutton(self, variable=var, value=-1, command=self.update_totals)
				abstainvote = ttk.Radiobutton(self, variable=var, value=0, command=self.update_totals)

				unvote.grid(row=firstrow+offset, column=2)
				yeavote.grid(row=firstrow+offset, column=3)
				nayvote.grid(row=firstrow+offset, column=4)
				abstainvote.grid(row=firstrow+offset, column=5)
				#menu = ttk.OptionMenu(self, var, -999, -999, 1, -1, 0)
				#menu.grid(row=firstrow+offset, column=1, columnspan=4)

				self.var_votes.append(var)
				self.input_votes.append([unvote, yeavote, nayvote, abstainvote])
				offset += 1
			offset += 1
		sep = ttk.Separator(self, orient=tk.HORIZONTAL)
		sep.grid(row=firstrow+offset, column=0, sticky=tk.E+tk.W, columnspan=6)
		offset += 1

		sep = ttk.Separator(self, orient=tk.VERTICAL)
		sep.grid(row=firstrow, column=1, sticky=tk.N+tk.S, rowspan=offset)

		self.label_totals = ttk.Label(self, text='TOTAL:')
		self.label_votercount = ttk.Label(self, text='(/{})'.format(self.proposalog.count_voters()))
		self.label_yea = ttk.Label(self, textvariable=self.var_yea)
		self.label_nay = ttk.Label(self, textvariable=self.var_nay)
		self.label_abstain = ttk.Label(self, textvariable=self.var_abstain)

		self.label_totals.grid(row=firstrow+offset, column=0, pady=8)
		self.label_votercount.grid(row=firstrow+offset, column=2)
		self.label_yea.grid(row=firstrow+offset, column=3)
		self.label_nay.grid(row=firstrow+offset, column=4)
		self.label_abstain.grid(row=firstrow+offset, column=5)
		offset += 1

		sep = ttk.Separator(self, orient=tk.HORIZONTAL)
		sep.grid(row=firstrow+offset, column=0, sticky=tk.E+tk.W, columnspan=6)
		offset += 1

		navbar = ttk.Frame(self)
		navbar.grid(row=firstrow+offset, column=0, columnspan=6)

		nav_back = ttk.Button(navbar, text='<- Previous', command=self.prev)
		nav_save = ttk.Button(navbar, text='Save', command=self.save)
		nav_exit = ttk.Button(navbar, text='Exit', command=self.exit)
		nav_next = ttk.Button(navbar, text='Next ->', command=self.next)

		nav_back.grid(row=0, column=0)
		nav_save.grid(row=0, column=1)
		nav_exit.grid(row=0, column=2)
		nav_next.grid(row=0, column=3)

		self.update_totals()

	def prev(self): pass
	def save(self): pass
	def exit(self): self.quit()
	def next(self): pass

	def update_totals(self):
		yea = 0
		nay = 0
		abstain = 0
		for var in self.var_votes:
			val = var.get()
			if val == -999: abstain += 1
			elif val > 0: yea += 1
			elif val < 0: nay += 1
			else: abstain += 1
		self.var_yea.set('+' + str(yea) if yea > 0 else str(yea))
		self.var_nay.set('-' + str(nay) if nay > 0 else str(nay))
		self.var_abstain.set(str(abstain))
