#!/usr/bin/env python

import os
import sqlite3
import warnings

class Type(object):
    def __init__(self, name, prefix='', suffix=''):
        self.name = name
        self.prefix = prefix
        self.suffix = suffix

    def __hash__(self): return hash((type(self), self.name, self.prefix, self.suffix))

class Voter(object):
    def __init__(self, id=None, name=None, type=None, active=True):
        self.name = name
        self.id = id
        self.type = type
        self.active = active

    def __hash__(self): return hash((type(self), self.name, self.type))

    def __repr__(self): return 'Voter({}, type={})'.format(self.name, self.type)

class Proposal(object):
    def __init__(self, id=None, code=None, name=None, description=None, mover=None, seconder=None, time=None, status=1, votes=None):
        self.id = id
        self.code = code
        self.name = name
        self.description = description
        self.mover = mover
        self.seconder = seconder
        self.time = time
        self.status = status

    def __hash__(self): return hash((type(self), self.code, self.name))

    def __repr__(self): return 'Proposal({}, status={})'.format(self.code, self.status)

class Proposalog(object):
    tables = ('votes', 'voters', 'proposals', 'types')

    def __init__(self, dbfile, time=0, voters=None, proposals=None, types=None, votes=None, propstatus=None): 
        fresh = not os.path.isfile(dbfile)
        self.connection = sqlite3.connect(dbfile)

        self.voters = [] if voters is None else voters
        self.proposals = [] if proposals is None else proposals
        self.types = [] if types is None else types
        self.votes = [] if votes is None else votes
        self.propstatus = propstatus

        if fresh: self.initialize()
        else: self.load()
        self.time = 0

    def add_type(self, name, prefix='', suffix=''):
        cursor = self.connection.cursor()
        self.connection.commit()
        cursor.execute('INSERT INTO types (name, prefix, suffix) VALUES (?, ?, ?)', (name, prefix, suffix))

    def add_voter(self, voter, type=None, active=True): 
        cursor = self.connection.cursor()
        if type is None:
            cursor.execute('INSERT INTO voters (name, type, active) VALUES (?, ?, ?);', (voter.name, self.resolve(voter.type, 'type'), active))
        else:
            cursor.execute('INSERT INTO voters (name, type, active) VALUES (?, ?, ?);', (voter, type, active))
    def remove_voter(self, voter): pass
    def add_proposal(self, proposal=None, code=None, name=None, description=None, time=None, mover=None, status=1, seconder=None): 
        cursor = self.connection.cursor()
        #cursor.executemany('INSERT INTO proposals (code, name, description, time, mover, seconder) VALUES (?, ?, ?, ?, ?, ?);', [proposal for proposal in self.proposals])
        if isinstance(proposal, Proposal):
            cursor.execute('INSERT INTO proposals (code, name, description, time, status, mover, seconder) VALUES (?, ?, ?, ?, ?, ?);', (
                proposal.code,
                proposal.name,
                proposal.description,
                proposal.time,
                proposal.status,
                self.get_id_by_name('voters', proposal.mover),
                self.get_id_by_name('voters', proposal.seconder),
            ))
        elif code is None:
            cursor.execute('INSERT INTO proposals (code, name, description, time, status, mover, seconder) VALUES (?, ?, ?, ?, ?, ?, ?);', (
                proposal,
                name,
                description,
                time,
                status,
                self.get_id_by_name('voters', mover),
                self.get_id_by_name('voters', seconder),
            ))
        elif proposal is None:
            cursor.execute('INSERT INTO proposals (code, name, description, time, status, mover, seconder) VALUES (?, ?, ?, ?, ?, ?, ?);', (
                code,
                name,
                description,
                time,
                status,
                self.get_id_by_name('voters', mover),
                self.get_id_by_name('voters', seconder),
            ))
    def remove_proposal(self, proposal): pass
    def remove_type(self, name): self.types.remove(name)

    def resolve(self, unknown, type, unique=False):
        if isinstance(unknown, int): return unknown

        searchfields = ['name']
        if type == 'proposal': 
            cl = Proposal
            tab = 'proposals'
            searchfields.append('code')
        elif type == 'vote': 
            cl = Vote
            tab = 'votes'
            searchfields = ['proposalid']
        elif type == 'voter': 
            cl = Voter
            tab = 'voters'
        elif type == 'type':
            cl = Type
            tab = 'types'
        else: raise ValueError('Unrecognized type: {}'.format(type))

        cursor = self.connection.cursor()

        results = []
        subresults = []
        for field in searchfields:
            subresult = set()
            if isinstance(unknown, str): 
                cursor.execute('SELECT rowid FROM {} WHERE {} = ?'.format(tab, field), (unknown,))
                cursor.execute('SELECT rowid FROM {} WHERE {} = ?'.format(tab, field), (unknown,))
                
            elif isinstance(unknown, cl):
                cursor.execute('SELECT rowid FROM {} WHERE {} = ?'.format(tab, field), (unknown.__getattribute__(field),))

            for x in cursor.fetchall():
                subresult.add(x[0])
            subresults.append(subresult)

        intersectionresult = subresults[0]
        for subresult in subresults[1:]: intersectionresult = intersectionresult.intersection(subresult)

        if unique:
            if len(intersectionresult) > 1: raise KeyError('Ambiguous keyword: {} in table {}'.format(unknown, tab))
            elif not intersectionresult: raise KeyError('Could not find any results for {} in table {}'.format(unknown, tab))
            else: return tuple(intersectionresult)[0]

        return list(intersectionresult)

    def add_vote(self, proposal, voter, vote): 
        
        proposalid = self.resolve(proposal, 'proposal', unique=True)
        voterid = self.resolve(voter, 'voter', unique=True)

        cursor = self.connection.cursor()

        #check for existing vote
        cursor.execute('SELECT vote FROM votes WHERE proposalid = ? AND voterid = ?;', (proposalid, voterid))
        if cursor.fetchall():
            cursor.execute('UPDATE votes SET vote = ? WHERE proposalid = ? AND voterid = ?;', (vote, proposalid, voterid))
        else:
            cursor.execute('INSERT INTO votes (proposalid, voterid, vote) VALUES (?, ?, ?);', (proposalid, voterid, vote))

    def get_id_by_name(self, table, name, unique=True):
        if name is None: return None if unique else []
        cursor = self.connection.cursor()
        if table not in self.tables: raise sqlite3.OperationalError('Table {} does not exist'.format(table))

        cursor.execute('SELECT rowid FROM {table} WHERE name = ?'.format(table=table), (name, ))
        results = cursor.fetchall()

        if len(results) == 0: 
            warnings.warn('No matches found for {} in {}'.format(name, table))
            return None
        elif unique:
            if len(results) == 1: return results[0][0]
            elif len(results) : raise ValueError('Non-unique name')
        else: return [r[0] for r in results]

    def initialize(self): 
        cursor = self.connection.cursor()

        cursor.execute('''CREATE TABLE types(
            name TEXT,
            prefix TEXT,
            suffix TEXT
        );''')
        cursor.execute('''CREATE TABLE voters(
            name TEXT,
            type TEXT,
            active BOOLEAN
        );''')
        cursor.execute('''CREATE TABLE proposals(
            code TEXT,
            name TEXT,
            time FLOAT,
            status INTEGER,
            mover INTEGER,
            seconder INTEGER,
            description TEXT
        );''')
        cursor.execute('''CREATE TABLE votes(
            voterid INTEGER,
            proposalid INTEGER,
            vote INTEGER
        );''')
        cursor.execute('''CREATE TABLE propstatus(
            name TEXT,
            alive BOOLEAN
        );''')

        if not self.types: self.types = [{'name':'Voter', 'prefix':'', 'suffix':''}]
        for t in self.types:
            self.add_type(**t)
        #cursor.executemany('INSERT INTO types (name) VALUES (?);', [[t] for t in self.types])

        if not self.propstatus: self.propstatus = [
            ('Active', True),
            ('Passed', False),
            ('Failed', False),
            ('Deadlock', True),
        ]
        cursor.executemany('INSERT INTO propstatus (name, alive) VALUES (?, ?);', self.propstatus)

        if self.voters: 
            for name in self.voters: self.add_voter(name, self.voters[name])
        if self.proposals: 
            for proposal in self.proposals: self.add_proposal(**proposal)

        if self.votes: 
            for voter, proposal, vote in self.votes: self.add_vote(voter, proposal, vote)
            #cursor.executemany('INSERT INTO votes (voterid, proposalid, vote) VALUES (?, ?);', [vote for vote in self.votes])

    def load(self): pass

    def save(self): self.connection.commit()

    def get_proposals(self, time=None, decay=1, include_dead=False):
        out = []
        for row in self.get_propids(time, decay, include_dead):
            out.append(self.get_proposal(row[0]))
        return out

    def get_propids(self, time=None, decay=1, include_dead=False):
        cursor = self.connection.cursor()
        if decay == 0 or time is None:
            if include_dead: cursor.execute('SELECT rowid FROM proposals;')
            else: cursor.execute('SELECT proposals.rowid FROM proposals JOIN propstatus ON (proposals.status = propstatus.rowid AND propstatus.alive = 1);')
        elif isinstance(type, int):
            if include_dead: cursor.execute('SELECT rowid FROM proposals WHERE time > ?;', (time - decay))
            else: cursor.execute('SELECT proposals.rowid FROM proposals JOIN propstatus ON (proposals.time > ? AND proposal.status = propstatus.rowid AND propstatus.alive = 1);', (time - decay))
        return cursor.fetchall()
    def count_proposals(self, time=None, decay=1, include_dead=False):
        return len(self.get_propids(time=time, decay=decay, include_dead=include_dead))

    def bbcode_report(self, time=None, decay=1, include_dead=False): 
        cursor = self.connection.cursor()

        out = ''
        def color(text, color): return '[color={}]{}[/color]'.format(color, text)
        def bold(text): return '[b]{}[/b]'.format(text)

        propids = self.get_propids(time=time, decay=decay, include_dead=include_dead)

        out = ''
        for row in propids:
            substr = ''
            proposal = self.get_proposal(row[0])
            votes = self.get_votes(proposalid=row[0])
            substr += '[quote={} {}]\n'.format(proposal.name, proposal.code)

            substr += 'Description: {}\n\n'.format(proposal.description)

            if proposal.mover is None: mover = ''
            else: mover = self.get_voter(proposal.mover).name
            substr += 'Proposed by {}\n'.format(mover)

            if proposal.seconder is None: seconder = ''
            else: seconder = self.get_voter(proposal.seconder).name
            substr += 'Seconded by {}\n\n'.format(seconder)

            by_type = {}
            yea = 0
            nay = 0
            abstain = 0
            for voter, vote in votes:
                if voter.type in by_type: by_type[voter.type].append((voter, vote))
                else: by_type[voter.type] = [(voter, vote)]
                if vote > 0: yea += 1
                elif vote < 0: nay += 1

            abstain = self.count_voters() - yea - nay
            substr += 'Votes: +{}/-{}/{}\n\n'.format(yea, nay, abstain)

            for typeid in by_type:
                t = self.get_type(typeid)
                for voter, vote in by_type[typeid]:
                    if vote > 0: votestr = 'votes ' + color('Yea', 'limegreen') 
                    elif vote < 0: votestr = 'votes ' + color('Nay', 'red') 
                    else: votestr = 'abstains'
                    substr += '{}{}{} {}\n'.format(t.prefix.title(), voter.name, t.suffix, votestr)
                substr += '\n'
            
            substr = substr.rstrip() + '\n'
            substr += '[/quote]'

            out += substr + '\n\n'
        return out

    def get_type(self, type):
        typeid = self.resolve(type, 'type', unique=True)
        cursor = self.connection.cursor()
        cursor.execute('SELECT name, prefix, suffix FROM types WHERE rowid = ?', (typeid,))
        result = cursor.fetchone()

        return Type(name=result[0], prefix=result[1], suffix=result[2])

    def get_voter(self, voter):
        voterid = self.resolve(voter, 'voter', unique=True)
        cursor = self.connection.cursor()
        cursor.execute('SELECT name, type FROM voters WHERE rowid = ?', (voterid,))
        result = cursor.fetchone()

        return Voter(id=voterid,
            name=result[0],
            type=result[1],
        )

    def get_votes(self, proposal=None, voter=None):
        cursor = self.connection.cursor()
        proposalid = self.resolve(proposal, 'proposal')
        proposalid = proposalid[0] if proposalid else None
        voterid = self.resolve(voter, 'voter')
        voterid = voterid[0] if voterid else None
        if proposal is None and voter is None:
            raise TypeError('Please specify a proposalid or a voterid')
        elif proposal is None:
            #given voterid, list proposals voted on
            cursor.execute('SELECT voterid, vote FROM votes WHERE voterid = ?', (voterid,))
            results = []
            voters = {}
            for voterid, vote in cursor.fetchall():
                if vote is None: continue
                if voterid not in voters: voters[voterid] = self.get_voter(voterid)
                results.append(voters[voterid], vote)
            return results
        elif voter is None:
            #given proposalid, list voters
            cursor.execute('SELECT voterid, vote FROM votes WHERE proposalid = ?', (proposalid,))
            voters = {}
            results = []
            for voterid, vote in cursor.fetchall():
                if vote is None: continue
                if voterid not in voters: voters[voterid] = self.get_voter(voterid)
                results.append((voters[voterid], vote))
            return results    
        else:
            cursor.execute('SELECT vote FROM votes WHERE proposalid = ? AND voterid = ?', (proposalid, voterid))
            result = cursor.fetchone()
            return result
                
    def get_proposal(self, query):
        proposalid = self.resolve(query, 'proposal', unique=True)
        cursor = self.connection.cursor()
        cursor.execute('SELECT * FROM proposals WHERE rowid = ?', (proposalid,))
        row = cursor.fetchone()

        return Proposal(
            id=proposalid,
            code=row[0],
            name=row[1],
            time=row[2],
            status=row[3],
            mover=row[4],
            seconder=row[5],
            description=row[6],
        )


    def dump(self): pass
    def count_voters(self, include_inactive=False):
        cursor = self.connection.cursor()
        if include_inactive: cursor.execute('SELECT rowid FROM voters')
        else: cursor.execute('SELECT rowid FROM voters WHERE active = 1')
        return len(cursor.fetchall())

    def get_voters(self, include_inactive=False):
        cursor = self.connection.cursor()
        if include_inactive: cursor.execute('SELECT rowid FROM voters')
        else: cursor.execute('SELECT rowid FROM voters WHERE active = 1')

        return [self.get_voter(row[0]) for row in cursor.fetchall()]

    def get_types(self):
        cursor = self.connection.cursor()
        cursor.execute('SELECT prefix, name, suffix FROM types')
        return [Type(name=name, prefix=prefix, suffix=suffix) for prefix, name, suffix in cursor.fetchall()]

    def truncate_table(self, table):
        cursor = self.connection.cursor()
        cursor.execute('DELETE FROM {}'.format(table))

    def amend_vote(self, proposal, voter, vote):
        cursor = self.connection.cursor()
        proposalid = self.resolve(proposal, 'proposal')[0]
        voterid = self.resolve(voter, 'voter')[0]

        cursor.execute('SELECT vote FROM votes WHERE proposalid = ? AND voterid = ?;', (proposalid, voterid))

        if cursor.fetchall():
            if vote is None: cursor.execute('DELETE FROM votes WHERE proposalid = ? AND voterid = ?', (proposalid, voterid))
            else: cursor.execute('UPDATE votes SET vote = ? WHERE proposalid = ? AND voterid = ?', (vote, proposalid, voterid))
        else:
            if vote is None: return
            else: cursor.execute('INSERT INTO votes (proposalid, voterid, vote) VALUES (?, ?, ?)', (proposalid, voterid, vote))

