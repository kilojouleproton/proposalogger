#!/usr/bin/env python

import proposalogger
import unittest
import os

class TestFreshDB(unittest.TestCase):
	dbfile = 'tmp_db.db'
	def dont_test_initialize(self):
		try: proposalog = proposalogger.Proposalog(self.dbfile)
		finally: os.remove(self.dbfile)

class TestSmallDB(unittest.TestCase):
	dbfile = 'tmp_db.db'
	types = [
		{'name':'Name', 'prefix':'the ', 'suffix':''},
		{'name':'High Priest', 'prefix':'', 'suffix':' (High Priest)'},
		{'name':'Angel', 'prefix':'', 'suffix':' (Angel)'},
		]
	voters = {
		'Zeus':'Name', 
		'Poseidon':'Name', 
		'Hades':'Name', 
		'Heracles':'Angel', 
		'Cassandra':'High Priest',
	}
	proposals = [
		{'code':'P01', 
			'name':'Divide the world', 
			'description':'Sets forth an equitable division of the world such that Zeus receives the skies, Poseidon the seas, Hades the underworld,and man the lands above', 
			'mover':'Zeus', 
			'seconder':'Poseidon'},
		{'code':'P02', 
			'name':'Ultimogeniture succession in Olympus', 
			'description':'The youngest child is hereby declared the primary heir and inherits all land and titles.', 
			'mover':'Zeus', 
			'seconder':None
		},
	]
	def test_voter_registration(self): 
		try: 
			proposalog = proposalogger.Proposalog(self.dbfile, 
				types=self.types,
				voters=self.voters,
			)
			cursor = proposalog.connection.cursor()
			cursor.execute('SELECT rowid FROM voters')
			assert len(cursor.fetchall()) == 5
			assert proposalog.get_id_by_name('voters', 'Hades') == 3

			proposalog.add_voter(voter='Hera', type='Name')
			cursor.execute('SELECT rowid FROM voters')
			assert len(cursor.fetchall()) == 6
			assert proposalog.get_id_by_name('voters', 'Hera') == 6
			proposalog.connection.commit()
		finally: 
			os.remove(self.dbfile)

	def test_voting(self):
		try: 
			proposalog = proposalogger.Proposalog(self.dbfile, 
				types=self.types,
				voters=self.voters,
				proposals=self.proposals,
			)
			#cursor = proposalog.connection.cursor()
			proposalog.add_vote(1, 'Zeus', 1)
			proposalog.add_vote('P02', 'Hades', -1)
			proposalog.add_vote(1, 'Heracles', 0)
			proposalog.add_vote(1, 'Cassandra', -1)
			proposalog.connection.commit()
		finally: 
			os.remove(self.dbfile)

	def test_bbcode(self):
		try:
			proposalog = proposalogger.Proposalog(self.dbfile, 
				types=self.types,
				voters=self.voters,
				proposals=self.proposals,
			)
			#cursor = proposalog.connection.cursor()
			proposalog.add_vote(1, 'Zeus', 1)
			proposalog.add_vote(1, 'Poseidon', 1)
			proposalog.add_vote(1, 'Heracles', 0)
			proposalog.add_vote(1, 'Cassandra', -1)
			proposalog.add_vote(1, 'Hades', 1)
			proposalog.add_vote('P02', 'Hades', -1)
			proposalog.save()

			proposalog.bbcode_report()
		finally:
			os.remove(self.dbfile)
			
	def test_load(self):
		try:
			proposalog1 = proposalogger.Proposalog(self.dbfile, 
				types=self.types,
				voters=self.voters,
				proposals=self.proposals,
			)
			proposalog1.save()
			proposalog2 = proposalogger.Proposalog(self.dbfile) 
			assert proposalog2.count_voters() == 5
			assert proposalog2.count_proposals(include_dead=True, decay=0) == 2

		finally: os.remove(self.dbfile)

	def test_inactive_voter(self):
		try:
			proposalog = proposalogger.Proposalog(self.dbfile, 
				types=self.types,
				voters=self.voters,
				proposals=self.proposals,
			)
			proposalog.add_voter(voter='Kronos', type='Name', active=False)
			assert proposalog.count_voters() == 5
			assert proposalog.count_voters(include_inactive=True) == 6
		finally: os.remove(self.dbfile)

if __name__ == '__main__': unittest.main()
